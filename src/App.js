import {useState} from "react"
import { createUserWithEmailAndPassword,
        onAuthStateChanged,
        signOut,
        signInWithEmailAndPassword
        } from 'firebase/auth'
import './Css.css'
import {auth} from './firebase'


const App = () => {
  // Register Data Store
  const [regEmail, setregEmail] = useState('');
  const [regPass, setregPass] = useState('')

  // Login Data Store 
  const [loginEmail, setloginEmail] = useState('')
  const [loginPass, setloginPass] = useState('')

  // Grab current User 
  const [user, setuser] = useState({})

  onAuthStateChanged(auth, (currentUser) => {
    setuser(currentUser)
  })

  // Register
  const ragister = async () => {
    try{
      const user = await createUserWithEmailAndPassword(
        auth, regEmail, regPass
      )
      console.log(user)
    } catch(err) {
      console.log(err.message)
    }

  }

  // Login
  const login = async () => {
    try{
      const user = await signInWithEmailAndPassword(
        auth, loginEmail, loginPass
      )
      console.log(user)
    } catch(err) {
      console.log(err.message)
    }
  }

  // Logotu
  const logout = async () => {
    await signOut(auth)
  }; console.log(user)
  return (
   <div className='hold'>
    <div>
      <h2> Register User</h2>
      <input onChange={(e) => setregEmail(e.target.value)} 
      type="text" placeholder=" Type Email..." />

      <input onChange={(e) => setregPass(e.target.value) }
      type="text" placeholder="Type Password..." />
      <button onClick={ragister}> Create User </button>
    </div>


    {/* Log in Area  */}
    <div>
      <h2> Log in </h2>
      <input onChange={(e)=> setloginEmail(e.target.value) }
      type="text" placeholder=" Type Email..." />

      <input onChange={(e)=> setloginPass(e.target.value)}
      type="text" placeholder="Type Password..." />
      <button onClick={login}> Log in </button>
    </div>

    <h4> User Logged In:</h4>
    <mark> {user?.email} </mark>
    <button onClick={logout}> Sign Out</button>
   </div>
  )
}

export default App